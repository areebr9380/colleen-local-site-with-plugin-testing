<?php

class ControllerExtensionPaymentEasypay extends Controller {
	public function index() {
	  
		$this->language->load('extension/payment/easypay');
		$this->load->model('checkout/order');
		
		$data['button_confirm'] = $this->language->get('button_confirm');
		
		$data['action'] = '';
		$live = trim($this->config->get('payment_easypay_live'));
		if($live) {
			$data['action'] = 'https://easypay.easypaisa.com.pk/easypay/Index.jsf';
		} else {
			$data['action'] = 'https://easypaystg.easypaisa.com.pk/easypay/Index.jsf';			
		}

		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
		if ($order_info) {
			$price = $order_info['total'];
			$orderAmount = floatval($price);
			if (strpos($orderAmount, '.') !== false) {
				$amount = $orderAmount;
			} else {
				$amount = sprintf("%0.1f",$orderAmount);
			}
			$hashKey = trim($this->config->get('payment_easypay_hashKey'));
			$autoRedirect = trim($this->config->get('payment_easypay_auto_redirection'));
			$custEmail = $order_info['email'];

			$expiryDate = '';
			$expiryDays = trim($this->config->get('payment_easypay_expiry'));
			$currentDate = new DateTime();
			
			if($expiryDays != null) {
				$currentDate->modify('+'.$expiryDays.'day');
				$expiryDate = $currentDate->format('Ymd His');
			}
						
			$custCell = html_entity_decode($order_info['telephone'], ENT_QUOTES, 'UTF-8');
			$orderId = $this->session->data['order_id'];
			$paymentMethodVal = trim($this->config->get('payment_easypay_payment_method'));
			$merchantConfirmPage = $this->url->link('extension/payment/easypay/callback', '', true);
			$storeId = trim($this->config->get('payment_easypay_storeId'));

			$hashRequest = '';
			if(strlen($hashKey) > 0 && (strlen($hashKey) == 16 || strlen($hashKey) == 24 || strlen($hashKey) == 32 )) {
					// Create Parameter map
					$paramMap = array();
					$paramMap['amount']  = $amount ;
					$paramMap['autoRedirect']  = $autoRedirect ;
					if($custEmail != null && $custEmail != '') {
						$paramMap['emailAddr']  = $custEmail ;
					}
					if($expiryDate != null && $expiryDate != '') {
						$paramMap['expiryDate'] = $expiryDate;
					}
					if($custCell != null && $custCell != '') {
						$paramMap['mobileNum'] = $custCell;
					}
					$paramMap['orderRefNum']  = $orderId ;
					
					if($paymentMethodVal != null && $paymentMethodVal != '') {
						$paramMap['paymentMethod']  = $paymentMethodVal ;
					}		
					$paramMap['postBackURL'] = $merchantConfirmPage;
					$paramMap['storeId']  = $storeId ;
					
					//Creating string to be encoded
					$mapString = '';
					foreach ($paramMap as $key => $val) {
						$mapString .=  $key.'='.$val.'&';
					}
					$mapString  = substr($mapString , 0, -1);
					
					// Encrypting mapString
					function pkcs5_pad($text, $blocksize) {
						
						$pad = $blocksize - (strlen($text) % $blocksize);
						return $text . str_repeat(chr($pad), $pad);
						
					}

					$alg = MCRYPT_RIJNDAEL_128; // AES
					$mode = MCRYPT_MODE_ECB; // ECB

					$iv_size = mcrypt_get_iv_size($alg, $mode);
					$block_size = mcrypt_get_block_size($alg, $mode);
					$iv = mcrypt_create_iv($iv_size, MCRYPT_DEV_URANDOM);	

					$mapString = pkcs5_pad($mapString, $block_size);
					$crypttext = mcrypt_encrypt($alg, $hashKey, $mapString, $mode, $iv);
					$hashRequest = base64_encode($crypttext);
			}
			
			$data['easypay_storeId'] = $storeId;
			$data['easypay_expiry'] = $expiryDate;
			$data['easypay_hashKey'] = $hashRequest;
			$data['easypay_payment_method'] = $paymentMethodVal;
			$data['easypay_auto_redirection'] = $autoRedirect;
			$data['orderid'] = $orderId;
			$data['orderamount'] = $amount;
			$data['billemail'] = $custEmail;
			$data['billphone'] = $custCell;
			
			$data['return_url'] = $this->url->link('extension/payment/easypay/callback', '', true);
			return $this->load->view('extension/payment/easypay', $data);
		}
	}
  
	public function callBack() {
		
		$this->load->model('checkout/order');
		
		$live = trim($this->config->get('payment_easypay_live'));
		if($live) {
			$data['action'] = 'https://easypay.easypaisa.com.pk/easypay/Confirm.jsf'; // Callback url for 2nd and final Redirection
		} else {
			$data['action'] = 'https://easypaystg.easypaisa.com.pk/easypay/Confirm.jsf';
		}
		
		$data['callbackurl'] = $this->url->link('extension/payment/easypay/confirm');
		
		if (isset($this->request->get['auth_token'])) {
			$data['authtoken'] = $this->request->get['auth_token'];
			header( 'Location: '.$data['action'].'?auth_token='.$data['authtoken'].'&postBackURL='.$data['callbackurl']);
		} else {
			$order_status_id = 10; // Order is failed.
			$this->load->model('checkout/order');
			$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $order_status_id);
			$this->response->redirect($this->url->link('checkout/failure'));
		}
	}
	
	public function confirm() {
		
		if (isset($this->request->get['status'])) { // Checking status received in 2nd Redirection to update order status is oc_order. This executes after 2ndRedirection
			error_log('Status in 2nd redirection: '.$this->request->get['status']);
			if (isset($this->request->get['orderRefNumber'])) {
				$order_id = $this->request->get['orderRefNumber'];
			}
			error_log('Order id after 2nd redirection: '.$order_id);
			$this->load->model('checkout/order');
			$order_info = $this->model_checkout_order->getOrder($order_id);
			if ($order_info) {
				if ($this->request->get['status'] == '0000') {
					$order_status_id = 15; // Order is successfully placed.
					$this->load->model('checkout/order');
					$this->model_checkout_order->addOrderHistory($order_id, $order_status_id);
				}
				if ($this->request->get['status'] == '0001') {
					$order_status_id = 10; // Order is failed.
					$this->load->model('checkout/order');
					$this->model_checkout_order->addOrderHistory($order_id, $order_status_id);
				}
			}
		} 
		
		else {// Redirect to Opencart success page. This executes when final redirection occurs (from Thank you page)
			$this->load->model('checkout/order');
			$order = $this->model_checkout_order->getOrder($this->session->data['order_id']);
			$order_status = $order['order_status_id'];
			if($order_status == 10 || $order_status == 0) { // to check if confirm was called at Thankyou/Failure page
				$this->response->redirect($this->url->link('checkout/failure'));
			} else {
				$this->response->redirect($this->url->link('checkout/success'));
			}
		}
		
	}
	
	public function easypayIPN() {
		if (isset($this->request->get['url'])) {
			$url = $this->request->get['url'];
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_URL, $url); 
			$output=curl_exec($curl);
			
			if($output != null) {
				$orderRefNumber = substr($url, strrpos($url, '/') + 1);
				$this->load->model('checkout/order');
				$order_info = $this->model_checkout_order->getOrder($orderRefNumber);
				if ($order_info) {
					//Parsing Response
					$jsonResponse = json_decode($output, true);
					if(isset($jsonResponse['transaction_status'])) {
						$transStatus = $jsonResponse['transaction_status'];
						
						if ($transStatus == 'DROPPED' || $transStatus == 'FAILED') {
							$order_status_id = 15;
							$this->load->model('checkout/order');
							$this->model_checkout_order->addOrderHistory($orderRefNumber, $order_status_id);
						}
						if ($transStatus == 'PAID') {
							$order_status_id = 5; //Complete
							$this->load->model('checkout/order');
							$this->model_checkout_order->addOrderHistory($orderRefNumber, $order_status_id);
						}
					} else {
						echo "Transaction status is not selected as IPN Param, hence order state is failed to set in order table";
					}
				}
			}
		}
	}
}
?>