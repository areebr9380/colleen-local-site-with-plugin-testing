<?php
class ControllerExtensionModuleIrsProductBasedShipping extends Controller {
	private $error = array();
       
  	public function index() {
		$this->language->load('extension/module/irs_productbasedshipping');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('module_irs_productbasedshipping', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}

		$data['user_token'] = $this->session->data['user_token'];

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		 if (isset($this->request->post['module_irs_productbasedshipping_status'])) {
		    $data['module_irs_productbasedshipping_status'] = $this->request->post['module_irs_productbasedshipping_status'];
		} else {
		    $data['module_irs_productbasedshipping_status'] = $this->config->get('module_irs_productbasedshipping_status');
		}
		
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
        );

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/irs_productbasedshipping', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/module/irs_productbasedshipping', 'user_token=' . $this->session->data['user_token'], true);

        $data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/irs_productbasedshipping', $data));
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/irs_productbasedshipping')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	 public function install() {
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "product_shipping_charge` (
  `product_shipping_charge_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(255) NOT NULL,
  `shipping_charge` decimal(15,4) NOT NULL DEFAULT '0.0000',
  		      PRIMARY KEY (`product_shipping_charge_id`)
                        )");
    }

   	public function uninstall() { 
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "product_shipping_charge`");      
        	$this->load->model('setting/setting');
	        $this->model_setting_setting->deleteSetting('productbasedshipping');
	}
}
?>