<?php
class ControllerExtensionPaymentEasypay extends Controller {
  private $error = array();
 
  public function index() {
    $this->language->load('extension/payment/easypay');
    $this->document->setTitle('Easypay Payment Method Configuration');
    $this->load->model('setting/setting');
	$test= 'lasjlda';
    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
      $this->model_setting_setting->editSetting('payment_easypay', $this->request->post);
      $this->session->data['success'] = 'Easypay configurations saved successfully';
      $this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true));
    }
	// Config Panel Fields
    $data['heading_title'] = $this->language->get('heading_title');
	$data['entry_status'] = $this->language->get('entry_status');
    $data['entry_storeId'] = $this->language->get('entry_storeId');
    $data['entry_expiry'] = $this->language->get('entry_expiry');
    $data['entry_hashKey'] = $this->language->get('entry_hashKey');
    $data['entry_pay_methods'] = $this->language->get('entry_payment_methods');
    $data['entry_autoRedirect'] = $this->language->get('entry_autoRedirect');
    $data['entry_env_live'] = $this->language->get('entry_live');
	
	$data['edit_settings'] = $this->language->get('text_edit');
	
	// Config Panel Btns
    $data['button_save'] = $this->language->get('save_btn');
    $data['button_cancel'] = $this->language->get('cancl_btn');

    $data['enabled'] = $this->language->get('enabled');
    $data['disabled'] = $this->language->get('disabled');
	
	$data['text_enabled'] = $this->language->get('text_enabled');
	$data['text_disabled'] = $this->language->get('text_disabled');
 
	//Actions for Config Panel Btns
    $data['action'] = $this->url->link('extension/payment/easypay', 'user_token=' . $this->session->data['user_token'], true);
    $data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true);
	
	//Breadcrumbs
	$data['breadcrumbs'] = array();

	$data['breadcrumbs'][] = array(
		'text' => $this->language->get('text_home'),
		'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
	);

	$data['breadcrumbs'][] = array(
		'text' => $this->language->get('text_extension'),
		'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true)
	);

	$data['breadcrumbs'][] = array(
		'text' => $this->language->get('heading_title'),
		'href' => $this->url->link('extension/payment/easypay', 'user_token=' . $this->session->data['user_token'], true)
	);
		
	if (isset($this->request->post['payment_easypay_status'])) {
		$data['payment_easypay_status'] = $this->request->post['payment_easypay_status'];
	} else {
		$data['payment_easypay_status'] = $this->config->get('payment_easypay_status');
	}
	
    if (isset($this->request->post['payment_easypay_storeId'])) {
      $data['payment_easypay_storeId'] = $this->request->post['payment_easypay_storeId'];
    } else {
      $data['payment_easypay_storeId'] = $this->config->get('payment_easypay_storeId');
    }
        
    if (isset($this->request->post['payment_easypay_expiry'])) {
      $data['payment_easypay_expiry'] = $this->request->post['payment_easypay_expiry'];
    } else {
      $data['payment_easypay_expiry'] = $this->config->get('payment_easypay_expiry');
    }
	
    if (isset($this->request->post['payment_easypay_hashKey'])) {
      $data['payment_easypay_hashKey'] = $this->request->post['payment_easypay_hashKey'];
    } else {
      $data['payment_easypay_hashKey'] = $this->config->get('payment_easypay_hashKey');
    }

    if (isset($this->request->post['payment_easypay_payment_method'])) {
      $data['payment_easypay_payment_method'] = $this->request->post['payment_easypay_payment_method'];
    } else {
      $data['payment_easypay_payment_method'] = $this->config->get('payment_easypay_payment_method');
    }

    if (isset($this->request->post['payment_easypay_live'])) {
      $data['payment_easypay_live'] = $this->request->post['payment_easypay_live'];
    } else {
      $data['payment_easypay_live'] = $this->config->get('payment_easypay_live');
    }

    if (isset($this->request->post['payment_easypay_auto_redirection'])) {
      $data['payment_easypay_auto_redirection'] = $this->request->post['payment_easypay_auto_redirection'];
    } else {
      $data['payment_easypay_auto_redirection'] = $this->config->get('payment_easypay_auto_redirection');
    }
 
    $this->load->model('localisation/order_status');
    $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

	$data['payment_easypay_methods'] = array();
    $data['payment_easypay_methods'] = $this->getPaymentMethods();

    //$this->template = 'extension/payment/easypay.tpl';
	
	$data['header'] = $this->load->controller('common/header');
	$data['column_left'] = $this->load->controller('common/column_left');
	$data['footer'] = $this->load->controller('common/footer');

	$this->response->setOutput($this->load->view('extension/payment/easypay', $data));
	
	/*$this->children = array(
	  'common/header',
	  'common/footer'
	);
 
	$this->response->setOutput($this->render());*/

  }

  public function getPaymentMethods() {

        return array(
            array('value'=>'', 'label'=>'All'),
            array('value'=>'CC_PAYMENT_METHOD', 'label'=>'Credit Card'),
            array('value'=>'MA_PAYMENT_METHOD', 'label'=>'Mobile Account'),            
            array('value'=>'OTC_PAYMENT_METHOD', 'label'=>'Over the counter'),                       
        );       
    }
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/payment/easypay')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}