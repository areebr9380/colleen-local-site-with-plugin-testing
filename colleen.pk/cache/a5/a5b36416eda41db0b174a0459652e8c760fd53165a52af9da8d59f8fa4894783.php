<?php

/* __string_template__30b4ea3b56403552657676b06a00ace016dbeb1fa9826b721a8c94f29031e6e6 */
class __TwigTemplate_ffdc14e66189c6fe4387239ca2697df5f593b94e3da61e96b5ee3a01f50bda8c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<footer>
  <div class=\"container\">
    <div class=\"row\">
      
      <div class=\"col-sm-3\">
        <a href=\"";
        // line 6
        echo (isset($context["home"]) ? $context["home"] : null);
        echo "\"><img src=\"icons/colleenlog.png\" style=\"width:230px;height:88px;\"/></a>
        <ul class=\"list-unstyled\">
        </ul>
      </div>

      <div class=\"col-sm-3\">
        <h5>";
        // line 12
        echo (isset($context["text_service"]) ? $context["text_service"] : null);
        echo "</h5>
        <ul class=\"list-unstyled\">
          <li><a href=\"";
        // line 14
        echo (isset($context["contact"]) ? $context["contact"] : null);
        echo "\">";
        echo (isset($context["text_contact"]) ? $context["text_contact"] : null);
        echo "</a></li>
          
          <li><a href=\"";
        // line 16
        echo (isset($context["sitemap"]) ? $context["sitemap"] : null);
        echo "\">";
        echo (isset($context["text_sitemap"]) ? $context["text_sitemap"] : null);
        echo "</a></li>
        </ul>
      </div>
      <div class=\"col-sm-3\">
        <h5>";
        // line 20
        echo (isset($context["text_extra"]) ? $context["text_extra"] : null);
        echo "</h5>
        <ul class=\"list-unstyled\">
          <li><a href=\"";
        // line 22
        echo (isset($context["manufacturer"]) ? $context["manufacturer"] : null);
        echo "\">";
        echo (isset($context["text_manufacturer"]) ? $context["text_manufacturer"] : null);
        echo "</a></li>
          <li><a href=\"";
        // line 23
        echo (isset($context["affiliate"]) ? $context["affiliate"] : null);
        echo "\">";
        echo (isset($context["text_affiliate"]) ? $context["text_affiliate"] : null);
        echo "</a></li>
          <li><a href=\"";
        // line 24
        echo (isset($context["special"]) ? $context["special"] : null);
        echo "\">";
        echo (isset($context["text_special"]) ? $context["text_special"] : null);
        echo "</a></li>
          ";
        // line 25
        if ((isset($context["informations"]) ? $context["informations"] : null)) {
            // line 26
            echo "       <ul class=\"list-unstyled\">
         ";
            // line 27
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["informations"]) ? $context["informations"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["information"]) {
                // line 28
                echo "          <li><a href=\"";
                echo $this->getAttribute($context["information"], "href", array());
                echo "\">";
                echo $this->getAttribute($context["information"], "title", array());
                echo "</a></li>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 30
            echo "        </ul>
      ";
        }
        // line 32
        echo "        </ul>
      </div>
      <div class=\"col-sm-3\">
        <h5>";
        // line 35
        echo (isset($context["text_account"]) ? $context["text_account"] : null);
        echo "</h5>
        <ul class=\"list-unstyled\">
          <li><a href=\"";
        // line 37
        echo (isset($context["account"]) ? $context["account"] : null);
        echo "\">";
        echo (isset($context["text_account"]) ? $context["text_account"] : null);
        echo "</a></li>
          <li><a href=\"";
        // line 38
        echo (isset($context["order"]) ? $context["order"] : null);
        echo "\">";
        echo (isset($context["text_order"]) ? $context["text_order"] : null);
        echo "</a></li>
          <li><a href=\"";
        // line 39
        echo (isset($context["wishlist"]) ? $context["wishlist"] : null);
        echo "\">";
        echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
        echo "</a></li>
          <li><a href=\"";
        // line 40
        echo (isset($context["newsletter"]) ? $context["newsletter"] : null);
        echo "\">";
        echo (isset($context["text_newsletter"]) ? $context["text_newsletter"] : null);
        echo "</a></li>
        </ul>
      </div>
      <p>";
        // line 43
        echo (isset($context["find_us"]) ? $context["find_us"] : null);
        echo "</p>
      <a href=\"http://www.facebook.com\" target=\"blank\"><img src=\"icons/fb.png\" style=\"width:30px;height:width:20px;\"/></a>
      <a href=\"http://www.twitter.com\" target=\"blank\"><img src=\"icons/twitter.png\" style=\"width:30px;height:width:20px;\"/></a>
      <a href=\"http://www.instagram.com\" target=\"blank\"><img src=\"icons/instagram.png\" style=\"width:30px;height:width:20px;\"/></a>
    </div>
    <hr>
    <p>Powered By <a href=\"http://www.ptnest.com\" target=\"blank\">PTNEST</a></p>
  </div>
</footer>
";
        // line 52
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["scripts"]) ? $context["scripts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            // line 53
            echo "<script src=\"";
            echo $context["script"];
            echo "\" type=\"text/javascript\"></script>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->
</body></html>";
    }

    public function getTemplateName()
    {
        return "__string_template__30b4ea3b56403552657676b06a00ace016dbeb1fa9826b721a8c94f29031e6e6";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  164 => 55,  155 => 53,  151 => 52,  139 => 43,  131 => 40,  125 => 39,  119 => 38,  113 => 37,  108 => 35,  103 => 32,  99 => 30,  88 => 28,  84 => 27,  81 => 26,  79 => 25,  73 => 24,  67 => 23,  61 => 22,  56 => 20,  47 => 16,  40 => 14,  35 => 12,  26 => 6,  19 => 1,);
    }
}
/* <footer>*/
/*   <div class="container">*/
/*     <div class="row">*/
/*       */
/*       <div class="col-sm-3">*/
/*         <a href="{{ home }}"><img src="icons/colleenlog.png" style="width:230px;height:88px;"/></a>*/
/*         <ul class="list-unstyled">*/
/*         </ul>*/
/*       </div>*/
/* */
/*       <div class="col-sm-3">*/
/*         <h5>{{ text_service }}</h5>*/
/*         <ul class="list-unstyled">*/
/*           <li><a href="{{ contact }}">{{ text_contact }}</a></li>*/
/*           */
/*           <li><a href="{{ sitemap }}">{{ text_sitemap }}</a></li>*/
/*         </ul>*/
/*       </div>*/
/*       <div class="col-sm-3">*/
/*         <h5>{{ text_extra }}</h5>*/
/*         <ul class="list-unstyled">*/
/*           <li><a href="{{ manufacturer }}">{{ text_manufacturer }}</a></li>*/
/*           <li><a href="{{ affiliate }}">{{ text_affiliate }}</a></li>*/
/*           <li><a href="{{ special }}">{{ text_special }}</a></li>*/
/*           {% if informations %}*/
/*        <ul class="list-unstyled">*/
/*          {% for information in informations %}*/
/*           <li><a href="{{ information.href }}">{{ information.title }}</a></li>*/
/*           {% endfor %}*/
/*         </ul>*/
/*       {% endif %}*/
/*         </ul>*/
/*       </div>*/
/*       <div class="col-sm-3">*/
/*         <h5>{{ text_account }}</h5>*/
/*         <ul class="list-unstyled">*/
/*           <li><a href="{{ account }}">{{ text_account }}</a></li>*/
/*           <li><a href="{{ order }}">{{ text_order }}</a></li>*/
/*           <li><a href="{{ wishlist }}">{{ text_wishlist }}</a></li>*/
/*           <li><a href="{{ newsletter }}">{{ text_newsletter }}</a></li>*/
/*         </ul>*/
/*       </div>*/
/*       <p>{{find_us}}</p>*/
/*       <a href="http://www.facebook.com" target="blank"><img src="icons/fb.png" style="width:30px;height:width:20px;"/></a>*/
/*       <a href="http://www.twitter.com" target="blank"><img src="icons/twitter.png" style="width:30px;height:width:20px;"/></a>*/
/*       <a href="http://www.instagram.com" target="blank"><img src="icons/instagram.png" style="width:30px;height:width:20px;"/></a>*/
/*     </div>*/
/*     <hr>*/
/*     <p>Powered By <a href="http://www.ptnest.com" target="blank">PTNEST</a></p>*/
/*   </div>*/
/* </footer>*/
/* {% for script in scripts %}*/
/* <script src="{{ script }}" type="text/javascript"></script>*/
/* {% endfor %}*/
/* <!--*/
/* OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.*/
/* Please donate via PayPal to donate@opencart.com*/
/* //-->*/
/* </body></html>*/
