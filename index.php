<?php
// Version
define('VERSION', '3.0.2.0');

// Configuration
session_start();
if(!isset($_SESSION['core'])){
	print "<script>window.open('login.php','_self');</script>";
}
if (is_file('config.php')) {
	require_once('config.php');
}

// Install
if (!defined('DIR_APPLICATION')) {
	header('Location: install/index.php');
	exit;
}

// Startup
require_once(DIR_SYSTEM . 'startup.php');

start('catalog');